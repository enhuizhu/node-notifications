const app = require('express')();
const http = require('http').Server(app);
const config = require('./config.json');
const ioService = require('./services/io.service');
const queueService = require('./services/queue.service');

app.get('/', (req, res) => {
  res.sendfile('index.html');
});

ioService.start(http);
queueService.start(config.default);

http.listen(7979, () => {
  console.log('[l] listen 7979');
});