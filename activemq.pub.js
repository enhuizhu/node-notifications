const queueConst = require('./constants/queuenames');
const acitveMqConfig = require('./config.json');
const Stomp = require('stomp-client');
const destination = `/queue/${queueConst.LOADED_TO_TEMPEST}`;
const client = new Stomp(acitveMqConfig.activeMq.host, acitveMqConfig.activeMq.port, acitveMqConfig.activeMq.user, acitveMqConfig.activeMq.pass);

client.connect(function(sessionId) {
  console.log('connect successfully!');
  const data = {id: 1};  
  client.publish(destination, JSON.stringify(data));
});