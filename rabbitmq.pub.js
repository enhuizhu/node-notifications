const amqp = require('amqplib/callback_api');
const rabbitMqConfig = require('./config.json');
const queueConst = require('./constants/queuenames');
const queue = `/queue/${queueConst.LOADED_TO_TEMPEST}`;

const user = rabbitMqConfig.rabbitMq.user;
const pass = rabbitMqConfig.rabbitMq.pass;
const host = rabbitMqConfig.rabbitMq.host;

amqp.connect(`amqp://${user}:${pass}@${host}`, function(err, conn) {
  conn.createChannel(function(err, ch) {
    console.log('[b] broadcast message');
    ch.assertQueue(queue, {durable: false});
    const data = {id: '4564654654'};
    ch.sendToQueue(queue, Buffer.from(JSON.stringify(data)));
  });
});