const notifications = require('./notifications.service');
const queueConst = require('../constants/queuenames');

class Io {
  static start(http) {
    this.io = require('socket.io')(http);
    this.sockets = [];
    this.setupListeners();
  }

  static setupListeners() {
    this.io.on('connection', (socket) => {
      this.sockets.push(socket);

      socket.on('disconnect', () => {
        console.log('[c] Socket has been disconnected');
        this.sockets = this.sockets.filter(so => so != socket);
      });
    });

    notifications.sub(queueConst.LOADED_TO_TEMPEST, (data) => {
      console.log('[r] IO get load to tempest events', data);
      this.io.emit(queueConst.LOADED_TO_TEMPEST, data);
    });
  }
}

module.exports = Io;