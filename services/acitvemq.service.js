const queueConst = require('../constants/queuenames');
const acitveMqConfig = require('../config.json');
const Stomp = require('stomp-client');
const destination = `/queue/${queueConst.LOADED_TO_TEMPEST}`;
const client = new Stomp(acitveMqConfig.activeMq.host, acitveMqConfig.activeMq.port, acitveMqConfig.activeMq.user, acitveMqConfig.activeMq.pass);
const notifications = require('./notifications.service');

class ActiveMq {
  static start() {
    client.connect(function(sessionId) {
      console.log('sessionId', sessionId);
      client.subscribe(destination, function(body, headers) {
        console.log('This is the body of a message on the subscribed queue:', body);
        notifications.pub(queueConst.LOADED_TO_TEMPEST, body);
      });
    });
  }
}

module.exports = ActiveMq;
