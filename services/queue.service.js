const activeMq = require('./acitvemq.service');
const rabbitMq = require('./rabbitmq.service');

class QueueServcie {
  static start(q) {
    switch(q) {
      case 'activeMq':
        activeMq.start();
        break;
      case 'rabbitMq':
        rabbitMq.start();
        break;
      default:
        throw `${q} is not supported, please check your configuration!`;
    }
  }
}

module.exports = QueueServcie;
