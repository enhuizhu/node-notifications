const queueConst = require('../constants/queuenames');
const amqp = require('amqplib/callback_api');
const rabbitMqConfig = require('../config.json');
const notifications = require('./notifications.service');
const queue = `/queue/${queueConst.LOADED_TO_TEMPEST}`;

class RabbitMq {
  static start() {
    const user = rabbitMqConfig.rabbitMq.user;
    const pass = rabbitMqConfig.rabbitMq.pass;
    const host = rabbitMqConfig.rabbitMq.host;
    
    amqp.connect(`amqp://${user}:${pass}@${host}`, function(err, conn) {
      if (err) {
        console.log('connction error:', err);
        return ;
      }

      console.log(`[c] connect to ${host} successfully!`);
    
      conn.createChannel(function(err, ch) {
        ch.assertQueue(queue, {durable: false});
        
        ch.consume(queue, function(msg) {
          console.log("[r] Received %s", msg.content.toString());
          notifications.pub(queueConst.LOADED_TO_TEMPEST, msg.content.toString());
        }, {noAck: true});
      });
    });
  }
}

module.exports = RabbitMq;
