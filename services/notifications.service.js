const EventEmitter = require('events');

class Notifications extends EventEmitter {}

const customizeNotification = new Notifications();

class myNotification {
  static sub(event, callback) {
    customizeNotification.on(event, callback);
  }

  static pub(event, data) {
    customizeNotification.emit(event, data);
  }
}

module.exports = myNotification;