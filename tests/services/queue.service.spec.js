const expect = require('chai').expect;
const sinon = require('sinon');
const queueServcie = require('../../services/queue.service');
const activeMq = require('../../services/acitvemq.service');
const rabbitMq = require('../../services/rabbitmq.service');

describe('queue service', () => {
  it('test the start function of queue service', () => {
    sinon.stub(activeMq, 'start');
    sinon.stub(rabbitMq, 'start');

    queueServcie.start('activeMq');
    expect(activeMq.start.called).to.be.true;

    queueServcie.start('rabbitMq');
    expect(rabbitMq.start.called).to.be.true;

    expect(() => {
      queueServcie.start('test');
    }).to.throw('test is not supported, please check your configuration!');
  });
});