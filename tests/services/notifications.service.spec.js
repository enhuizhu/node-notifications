const expect = require('chai').expect;
const notifications = require('../../services/notifications.service');

describe('notifications service', () => {
  it('test sub', () => {
    let testData = 'test data';

    notifications.sub('test', (data) => {
      expect(data).to.equal(testData);
    });

    notifications.pub('test', testData);
  });
});
